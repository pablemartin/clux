El club quiere:

- Tener una web: velezsarsfield.com.ar
- Tener una app donde compartir informacion de sus actividades
entre los socios y otros usuarios visitates, por ejemplo una red social.
- Tener una app que le permita hacer una gestion de club.


El usuario del club quiere:

- Acceder y compartir informacion de cada actividad.
- Ver informacion de otros clubes.
- Otros que puede querer:
    - Mostrar su perfil deportivo o social.
        - Mostrar su estado: en que club y que rol.
        - Mostrar sus trofeos
        
        
Acciones por usuario:

Registra un club (no self-host)
Registra una actividad
Registra miembros de club
Registra miembros de actividad
Colabora con la historia del club
Registra identidad del club
Registra eventos de club
Registra eventos de actividad
Registra clases de actividad
Registra instalaciones segun tipo de actividad y club
Sube videos y fotos de club
Sube videos y fotos de actividad
Sube noticia/comentario tipo microblog (feed)
Sigue un club
Comenta publicaciones de club
Registra categorias de actividad
Registra tarifas de actividad
Registra partidos de actividad
Registra torneos de actividad
Registra trofeos de torneos de actividad
Configura su nivel de privacidad

Acciones modo cli

clux    add user
        add club
        add activity
        club add activity
        club add user
        club add history
        club add instalacion/facility
        club add estatuto
        club add reglamento

    
